#!/bin/bash

name_file=""
name_file+="Tens_Batt_"
name_file+=$(date -u +%Y_%m)
date=$(date -u +%Y-%m-%d\ %H:%M)


if [ -e $name_file ]
then
	tens_digit+=$(cat /sys/bus/iio/devices/iio\:device0/in_voltage0_raw)
   	tens_analog=$(echo "scale=3;1.8*$tens_digit/4095" | bc -l)
	tens_batt=$(echo "scale=2;($tens_analog*(1000000+120000))/120000" |bc -l)
	ligne+=$date
	ligne+=";"
	ligne+=$tens_batt
	echo $ligne >> /opt/data/$name_file
else
	touch /opt/data/$name_file
	tens_digit+=$(cat /sys/bus/iio/devices/iio\:device0/in_voltage0_raw)
        tens_analog=$(echo "scale=3;1.8*$tens_digit/4095" | bc -l)
        tens_batt=$(echo "scale=2;($tens_analog*(1000000+120000))/120000" |bc -l)
        ligne+=$date
        ligne+=";"
        ligne+=$tens_batt
        echo $ligne >> /opt/data/$name_file

fi

exit
