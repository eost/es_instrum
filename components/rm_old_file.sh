#!/bin/bash

find /opt/data/archive/meteo -name "*.csv" -type f -mtime +30 -exec rm {} \;
find /opt/data/archive/gnss -name "*.rnx.gz" -type f -mtime +30 -exec rm {} \;
