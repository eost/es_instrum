#!/bin/bash

#get the sitename
if [ -e /opt/bin/sitename.txt ]
then
    SITE=$(cat /opt/bin/sitename.txt)
else
    SITE="GNSS"
fi
#get the coordinates
if [ -e /opt/bin/coordinates.txt ]
then
	coordinates=$(cat /opt/bin/gps/coordinates.txt)
else
	coordinates=$(gpspipe -w -n 10 | grep -m 1 lon | awk '{split($0,a,","); print substr(a[7],7,10) " " substr(a[8],7,10) " " substr(a[9],7,10)}')
fi
#run str2str ntrip server to gnssfr Caster if not loaded
pgrep str2str > /dev/null 2>&1 || /opt/bin/gps/str2str -in tcpcli://127.0.0.1:5001#ubx -opt "-tt 0.025 -TADJ=0.05" -out tcpsvr://127.0.0.1:5002 -out ntrips://gpsg:regal\!@134.59.147.34:2101/$SITE#rtcm3 -msg "1006(10),1008(10),1019(30),1020(30),1033(10),1042(30),1046(30),1077(1),1087(1),1097(1),1107(1),1127(1),1230(10)" -p $coordinates -a AS-ANT2BCAL > /dev/null 2>&1 &
