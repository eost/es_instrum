#!/usr/bin/python2.7
import serial
from time import time, gmtime, strftime, sleep
from os import path, getpid, getppid, mkdir
from subprocess import Popen, PIPE


#variables
port="/dev/gnss"
baud=460800
gnss_path="/dev/shm"
leapseconde=18
gps_id=0
debug=1

#check script is not currently in process
ps=Popen(["ps", "-ef"], stdout=PIPE)
for line in ps.communicate()[0].splitlines():
        #if "ublox_store.py" in line and "python" in line and str(getpid()) not in line and str(getppid()) not in line:
        if "ublox_store.py" in line and "python" in line and str(getpid()) not in line :
                    print line,"ublox_store.py already in process, Exiting..."
                    exit()

#open serial port
ser = serial.Serial(port,baud,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS,timeout=1,rtscts=False)
ser.flush();

def reset():
    #calculate UBX checksum
    Buffer=(0x06,0x09,0x0d,0x00,0xff,0xfb,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0x00,0x00,0x17)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A%256,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
    print "GNSS: reset receiver to defaul parameter"
    ser.write(ubx_trame.decode("hex"))
    ser.flush();
    

def configure_obs_freq(freq):
    #calculate UBX checksum
    Buffer=(0x06,0x01,0x03,0x00,0x02,0x15,freq)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "GNSS: %ds UBX-RAWX rate request" % freq
    ser.write(ubx_trame.decode("hex"))

def configure_nav_freq(freq):
    #calculate UBX checksum
    Buffer=(0x06,0x01,0x03,0x00,0x02,0x13,freq)
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "GNSS: %ds UBX-SFRBX rate request" % freq
    ser.write(ubx_trame.decode("hex"))

def configure_acq_rate():
    # obs rate=50ms,nav cycle=20,time system=GPS
    # obs=20Hz, nav=1Hz
    #        CFG-PRT | 06 bits | obs rate| nav rate| sys time
    Buffer=(0x06,0x08,0x06,0x00,0x32,0x00,0x14,0x00,0x01,0x00)
    #calculate UBX checksum
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "GNSS: acquisition rate 100ms request" 
    ser.write(ubx_trame.decode("hex"))

def configure_usb_proto():
    # in=RTCM3+UBX, out=UBX
    #        CFG-PRT | 20 bits | USB|  NC|  TX pin | NC    NC   NC   NC   NC   NC   NC   NC| IN proto|OUT proto|  NC   NC   NC   NC
    Buffer=(0x06,0x00,0x14,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x33,0x00,0x01,0x00,0x00,0x00,0x00,0x00)
    #calculate UBX checksum
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    if debug>0:
        print "GNSS: USB protocole UBX request" 
    ser.write(ubx_trame.decode("hex"))

def configure_uart_proto(serial_baud):
    if(serial_baud==9600):
        # in=none, out=NMEA
        # Bitfield=8N1
        # Baud=9600
        #        CFG-PRT | 20 bits | S2 |  NC|  TX pin | BitField Mode     | Baud              | IN proto|OUT proto|  NC   NC   NC   NC
        Buffer=(0x06,0x00,0x14,0x00,0x02,0x00,0x00,0x00,0xc0,0x08,0x00,0x00,0x80,0x25,0x00,0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x00,0x00)
        if debug>0:
            print "# GNSS: UART at %dbaud with NMEA protocole request" % serial_baud
    elif(serial_baud==38400):
        # in=RTCM3+UBX, out=UBX+NMEA
        #        CFG-PRT | 20 bits | S2 |  NC|  TX pin | BitField Mode     | Baud              | IN proto|OUT proto|  NC   NC   NC   NC
        Buffer=(0x06,0x00,0x14,0x00,0x02,0x00,0x00,0x00,0xc0,0x08,0x00,0x00,0xc0,0x96,0x00,0x00,0x33,0x00,0x03,0x00,0x00,0x00,0x00,0x00)
        if debug>0:
            print "# GNSS: UART at %dbaud with UBX+NMEA protocole request" % serial_baud
    elif(serial_baud==115200):
        # in=RTCM3+UBX, out=UBX+NMEA
        #        CFG-PRT | 20 bits | S2 |  NC|  TX pin | BitField Mode     | Baud              | IN proto|OUT proto|  NC   NC   NC   NC
        Buffer=(0x06,0x00,0x14,0x00,0x02,0x00,0x00,0x00,0xc0,0x08,0x00,0x00,0x00,0xc2,0x01,0x00,0x33,0x00,0x03,0x00,0x00,0x00,0x00,0x00)
        if debug>0:
            print "# GNSS: UART at %dbaud with UBX+NMEA protocole request" % serial_baud
    elif(serial_baud==460800):
        # in=RTCM3+UBX, out=UBX+NMEA
        #        CFG-PRT | 20 bits | S2 |  NC|  TX pin | BitField Mode     | Baud              | IN proto|OUT proto|  NC   NC   NC   NC
        Buffer=(0x06,0x00,0x14,0x00,0x02,0x00,0x00,0x00,0xc0,0x08,0x00,0x00,0x00,0x07,0x08,0x00,0x33,0x00,0x03,0x00,0x00,0x00,0x00,0x00)
        if debug>0:
            print "# GNSS: UART at %dbaud with UBX+NMEA protocole request" % serial_baud
    else:
        print "# GNSS: UART %dbaud is not in the list (9600, 38400, 115200, 460800)\n\tPlease select the good serial speed\n" % serial_baud
    #calculate UBX checksum
    CK_A = 0
    CK_B = 0
    for I in range(len(Buffer)):
        CK_A = CK_A + Buffer[I]
        CK_B = CK_B + CK_A
    Buffer=(0xB5,0x62)+Buffer+(CK_A%256,CK_B%256)
    ubx_trame= "".join("{:02x}".format(c) for c in Buffer)

    if debug>1:
        print ubx_trame
    ser.write(ubx_trame.decode("hex"))
    ser.baudrate=serial_baud
    ser.flush

def write(gps_id, S, ext):
        date=strftime("_%Y%m%d%H00",gmtime(time()+leapseconde+1))
        filename=path.join(gnss_path,'GPS'+str(gps_id)+date+ext)
        #print "writing data in %s" % filename
        file=open(filename,'a')
        file.write(S)
        file.close()

########
# main #
########

#initialize buffer and counter
buff=''
read_bits=0
no_data_counter=0
reset()
sleep(0.1)
configure_usb_proto()
sleep(0.1)
configure_uart_proto(9600)
sleep(0.1)
configure_acq_rate()
sleep(0.1)
configure_obs_freq(1)
sleep(0.1)
configure_nav_freq(1)

#main loop
while True:
        #get serial queue
        bits=ser.inWaiting()
        #read serial and store data
        if bits:
            buff+=ser.read(bits)
            read_bits+=bits
            no_data_counter=0
        #when queue is empty
        else:
            sleep(0.01)
            no_data_counter+=1
            #proceed data when not data readed since several times 
            if (no_data_counter>9 and buff) or read_bits > 4096:
                if debug>0:
                    print "read %d bits at %s" % (read_bits,strftime("%Y-%m-%d %H:%M:%S",gmtime()))
                if debug>1:
                    print(buff)
                #write data to dated file
                write(gps_id,buff,'.ubx')
                #re-initialise buffer
                buff=''
                read_bits=0
ser.close()


