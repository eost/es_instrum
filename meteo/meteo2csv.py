#!/usr/bin/python3

import serial
import time
import math
import board
import adafruit_bmp3xx
import os
from subprocess import Popen, PIPE
import datetime

ser = serial.Serial('/dev/ttyS4', 4800)
i2c = board.I2C()
bmp = adafruit_bmp3xx.BMP3XX_I2C(i2c)

sample_rec = 0
sample_rec_bmp = 0
rec_number = 0
start_time = ""
after_time = ""
err = -1
debug = 0

temp = -111
temp_in = -111
pressure = -1
rh = -1
uv = -1
solar = -1
windspd = -1
windspd_gust = -1
windspd_mean = -1
windspd_min = -1
windir = -1
windir_gust = -1
windir_mean = -1
windir_min = -1

rain_rec = -1 #Valeur du nombre de basculement sans le bit de boot de la station
rain_bit = -1 #Valeur du bit de boot de la station
rain_rec_old = -1 #Valeur du nombre de basculement de l'itération précédente
rain_bit_old = -1 #Valeur du bit de boot de l'itération précedente

rain_rate = 0 #Taux de pluie en mm/h
rain_cumul = 0 #Taux de pluie en mm deuis le démarrage du programme
tips = 0 #nombre de basculement durant la minute


#Fonction pour lire le nom de la station
if os.path.isfile('/opt/bin/sitename.txt'):
    site=open('/opt/bin/sitename.txt','rb').read().decode('utf8')[:4]
else:
    site='METEO'

#Fonction pour vérifier si le programme tourne déjà
ps=Popen(["ps", "-ef"], stdout=PIPE)
for line in ps.stdout.read().decode("utf-8").splitlines():
    if "console2csv.py" in line and "python" in line and str(os.getpid()) not in line:
        if debug:
            print(line,"console2csv.py already in process, Exiting...")
            exit()

meteo_path='/opt/data/archive/meteo'
ext = ".csv"
today= datetime.datetime.today().strftime("_%Y%m%d0000")
filename=os.path.join(meteo_path,site+today+ext)

#Fonction pour créer le chemin météo dans l'arborescence
if not os.path.isdir(meteo_path):
    os.makedirs(meteo_path)

#Fonction de vérification de la pression
def check_pressure(p): #Pression en Pa
    if p < 1200 and p > 700 and p != 782.960078125:
        return True
    else:
        return False

#Fonction de vérification de la température
def check_temperature(t): #Température en °C
    if t < 65 and t > -25:
        return True
    else:
        return False

#Fonction de moyenne des valeurs
#Simple fonction itérative
#Pour utiliser moins d'espace mémoire, cette fonction ne prend pas de paramètres tableau
#Les paremètres sont la dernière valeur moyenne qui sera mise à jour, la nouvelle valeur et le nombre d'itération précédente
def mean(old_value, new_value, nb_value):
    if nb_value > 0:
        return ((old_value*nb_value+new_value)/(nb_value+1))
    else:
        return new_value

#Fonction itérative pour les moyennes des valeurs angulaires
def angular_mean(avg, val, count):
    if count == 0:
        return val
    avg = math.radians(avg)
    val = math.radians(val)
    avg_rad = math.atan2(math.sin(avg), math.cos(avg))
    new_rad = math.atan2(math.sin(val), math.cos(val))
    mean = math.degrees((avg_rad*(count) + new_rad)/(count+1))
    if mean < 0:
        mean += 360
    return mean


#Fonction pour écrire le header au début de chaque fichier
def write_header():
    with open(filename, "w") as csvfile:
        csvfile.write('#DateTime(UTC);Rec_nb(Minute);WindSpeed_min(km/h);WindSpeed_gust(km/h);WindSpeed_avg(km/h);WindDir_Min(Deg);WindDir_gust(Deg);WindDir_avg(Deg);Temp(Deg_C);RH(%);RainRate(mm/min);RainCumul(mm);UV(MEDs);Solar_rad(W/m2);Temp_in(Deg_C);Pressure(hPa);Nb_samples;Error_status(-1:boot|0:normal|%1:rain_error|%10:pressure_errors|%1000:ISS_rebooot)\n')


#Fonction pour écrire les données dans le fichier CSV
def write_data():
    with open(filename, "a") as csvfile:
        csvfile.write(f"{datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')};{rec_number};{windspd_min:.2f};{windspd_gust:.2f};{windspd_mean:.2f};{windir_min:.0f};{windir_gust:.0f};{windir_mean:.0f};{temp:.2f};{rh:.0f};{rain_rate:.1f};{(rain_cumul*0.2):.1f};{uv};{solar};{temp_in:.2f};{pressure:.1f};{sample_rec};{err}\n")


#Fonction pour commencer à écouter les trames à la seconde 0
start_time = datetime.datetime.today().strftime('%M%S')
while datetime.datetime.today().strftime('%S') != '00':
    time.sleep(0.1)
    start_time = datetime.datetime.today().strftime('%M%S')
    after_time = datetime.datetime.today()+datetime.timedelta(minutes=1)
    after_time = after_time.strftime('%M%S')

##########
## MAIN ##
##########
while True:

    #Lectures des bits dans le buffer
    bits = ser.inWaiting()
    if bits > 8:
        trash = ser.read(bits)

    if bits == 8:
        frame = list(ser.read(bits))
        
        #Transformation des bits en octets hexadécimaux
        for i in range(len(frame)):
            frame[i]=hex(frame[i]).split('x')[-1].zfill(2)

        #Récupération de la vitesse du vent
        windspd = int(frame[1], 16)*1.61
        #Récupération de la direction du vent
        if int(frame[2], 16) == 0:
            windir = 360
        else:
            windir = 9+int(frame[2], 16)*342/255

        #Récupération de la vitesse et de la direction des rafales
        if windspd > windspd_gust:
            windspd_gust = windspd
            windir_gust = windir

        #Récupération du vent minute
        if windspd < windspd_min or windspd_min == -1:
            windspd_min = windspd
            windir_min = windir

        #Mise à jour des moyennes des vents
        windspd_mean=mean(windspd_mean, windspd, sample_rec)
        windir_mean = angular_mean(windir_mean, windir, sample_rec)

        #Mise à jour des données de pression et de température du bmp388
        buff = bmp.pressure
        buff2 = bmp.temperature
        if check_pressure(buff) and check_temperature(buff2):
            pressure = mean(pressure, buff, sample_rec_bmp)
            temp_in = mean(temp_in, buff2, sample_rec_bmp)
            sample_rec_bmp+=1
        else:
            err+=10

        sample_rec+=1

        #Récupération des données depuis la trame
        if frame[0] == '80': #Récupération des données température
            val = frame[3]+frame[4]
            val = int(frame[3]+frame[4], 16) / 16
            temp = ((val/10)-32)*(5/9)
            continue

        elif frame[0] == '40': #récupération des données UV
            val = int(bin(int(frame[3]+frame[4], 16)), 2) >>6
            if hex(val) < '0x3ff':
                uv = val/50
            else:
                uv = -1
            continue

        elif frame[0] == '60': #Récupération des données Solaire
            val = int(bin(int(frame[3]+frame[4], 16)), 2) >>6
            if hex(val) < '0x3fe':
                solar = val*1.757936
            else:
                solar = -1
            continue

        elif frame[0] == 'a0': #Récupération des données d'humidité
            val = (((int(frame[4], 16) >> 4) << 8) + int(frame[3], 16)) /10
            rh = val
            continue

        elif frame[0] == 'e0': #Récupération des données de pluie
            
            value = int(frame[3], 16)
            rain_bit = value >> 7
            if rain_bit == 1:
                rain_rec = value - 128
            else:
                rain_rec = value

            #Traitement des différent cas de configuration des données de pluie
            #Cas 1 : Boot du programme
            if rain_bit_old == -1:
                rain_cumul = 0
                tips = 0
                rain_rate = 0
                pass
                
            #Cas 2 : Reboot de la station
            elif rain_bit == 1 and rain_rec_old > rain_rec:
                tips = tips + rain_rec
                rain_rate = tips*0.2
                rain_cumul = rain_cumul + rain_rec
                err+=1000
                pass
                
            #Cas 3 : La station tourne sans changer son bit
            elif rain_bit == rain_bit_old and rain_rec >= rain_rec_old:
                tips = tips + (rain_rec - rain_rec_old)
                rain_rate = tips*0.2
                rain_cumul = rain_cumul + (rain_rec - rain_rec_old)
                pass

            #Cas 4 : La station ne change pas son bit mais retourne 0
            elif rain_bit == rain_bit_old and rain_rec < rain_rec_old:
                tips = tips + (128 - rain_rec_old) + rain_rec
                rain_rate = tips*0.2
                rain_cumul = rain_cumul + (128 - rain_rec_old) + rain_rec
                pass

            #Cas 5 : La station change de bit de 1 à 0
            elif rain_bit == 0 and rain_bit_old == 1:
                tips = tips + rain_rec + (128 - rain_rec_old)
                rain_rate = tips*0.2
                rain_cumul = rain_cumul + rain_rec + (128 - rain_rec_old)
                pass
                
            #Calcule des valeurs de rainrec et rain_bit pour l'itération suivante
            rain_bit_old = value >> 7
            if rain_bit_old == 1:
                rain_rec_old = value - 128
            else:
                rain_rec_old = value
            continue

    #Fonction pour écrire les données à la seconde 0
    start_time = datetime.datetime.today().strftime('%M%S')
    if start_time == after_time:
        today= datetime.datetime.today().strftime("_%Y%m%d0000")
        filename = os.path.join(meteo_path,site+today+ext)
        if not os.path.isfile(filename):
            write_header()
            write_data()
        else:
            write_data()
            
        rec_number +=1
        
        #Réinistialisation des variables
        sample_rec = 0
        sample_rec_bmp = 0
        err = 0

        temp = -111
        temp_in = -111
        pressure = -1
        rh = -1
        uv = -1
        solar = -1
        windspd = -1
        windspd_gust = -1
        windspd_mean = -1
        windspd_min = -1
        windir = -1
        windir_gust = -1
        windir_mean = -1
        windir_min = -1
        rain_rate = 0
        tips = 0

        after_time = datetime.datetime.today()+datetime.timedelta(minutes=1)
        after_time = after_time.strftime('%M%S')
        
    time.sleep(0.05)
    frame = []


