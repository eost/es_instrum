#!/bin/bash

echo "Enter the site name (4 Capital letter like EVSA):"
read sitename
echo $sitename > /opt/bin/sitename.txt

#Set cron to store battery tension
if [ "$(crontab -u debian -l | grep /opt/bin/components/tens_batt.sh)" == "" ]
then
    echo "Add /opt/bin/tens_batt.sh (every 5 minutes) to contab"
    (crontab -u debian -l; echo "*/5 * * * * /opt/bin/components/tens_batt.sh" ) | crontab -u debian -
fi

#Set con to transfert data to OMIV server
if [ "$(crontab -u debian -l | grep rsync)" == "" ]
then
    echo "Add rsync data backup twice a day to contab"
    (crontab -u debian -l; echo "0 1,13 * * * rsync -az /opt/data/archive/ debian@185.155.93.115:/data/$sitename" ) | crontab -u debian -
fi

#Set cron to delete old file in data archive
if [ "$(crontab -u debian -l | grep /opt/bin/components/rm_old_file.sh)" == "" ]
then
    echo "Add /opt/bin/rm_old_files (every day) to contab"
    (crontab -u debian -l; echo "0 12 * * * /opt/bin/components/rm_old_file.sh" ) | crontab -u debian -
fi

#Set cron to store rinex archive in data directory
if [ "$(crontab -u debian -l | grep /opt/bin/gps/archive_rinex.sh)" == "" ]
then
    echo "Add /opt/bin/gps/archive_rinex.sh (every hour) to contab"
    (crontab -u debian -l; echo "0 * * * * /opt/bin/gps/archive_rinex.sh" ) | crontab -u debian -
fi

#Set the BB port configuration
echo "Add data tree overlay file to open UART1 port and add /dev/pps0"
sudo cp /opt/bin/config/BB-ENVIRO-UART1-2-4-PPS-00A0.dtbo /lib/firmware/
sudo cat /opt/bin/config/uEnv.txt > /boot/uEnv.txt

#Set BB network configuration
echo "Set static IP of the BeagleBone"
sudo cat /opt/bin/config/interfaces > /etc/network/interfaces

#Set gpsd configuration
echo "Set gpsd configuration"
sudo cat /opt/bin/config/gpsd > /etc/default/gpsd

#Set connection between ntp and gpsd
echo "Set connection between ntp and gpsd"
sudo cat /opt/bin/config/ntp.conf > /etc/ntp.conf

#Put ssh key in good directory
mkdir /home/debian/.ssh
sudo chown -R debian:debian /home/debian/.ssh
cp /opt/bin/config/id_rsa /opt/bin/config/id_rsa.pub /home/debian/.ssh/
ssh-keyscan -H 185.155.93.115 >> /home/debian/.ssh/known_hosts

#Change owner of /dev/shm and /opt/data
sudo chown debian:debian /dev/shm
sudo mkdir /opt/data
sudo chown debian:debian /opt/data

#Set IPV6 gpsd conf and install rsync
echo "enable IPv6 to loopback interface for gpsd tcp socket connexion"
sudo bash -c 'echo "net.ipv6.conf.lo.disable_ipv6 = 0" >> /etc/sysctl.conf'
sudo sysctl -p
echo "Install rsync"
sudo apt install -y rsync

#Make a backup of service config directory
sudo cp -r /etc/systemd/system /etc/systemd/system.save

#Set up service on BB
sudo ln -s /opt/bin/service/meteo.service /etc/systemd/system/meteo.service
sudo ln -s /opt/bin/service/modem.service /etc/systemd/system/modem.service
sudo ln -s /opt/bin/service/ntripserver.service /etc/systemd/system/ntripserver.service
sudo ln -s /opt/bin/service/ublox.service /etc/systemd/system/ublox.service

sudo systemctl enable meteo.service
sudo systemctl enable modem.service
sudo systemctl enable ntripserver.service
sudo systemctl enable ublox.service

sudo systemctl start meteo.service
sudo systemctl start modem.service
sudo systemctl start ntripserver.service
sudo systemctl start ublox.service
